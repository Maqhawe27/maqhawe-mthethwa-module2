// Defining class
class WinningApp {
  var name;
  var sector;
  var developer;
  var year;

  // Defining class function
  showWinningAppInfo() {
    print("The name of the app is: $name");
    print("From this sector/category: $sector");
    print("Developers name is: $developer");
    print("The Year of them winning was: $year");
  }
}

void main() {
  // creating object called wa
  var wa = new WinningApp();
  wa.name = "WumDrop";
  wa.sector = "enterprise";
  wa.developer = "Simon Hartley";
  wa.year = 2015;

  //accessing class  function
  wa.showWinningAppInfo();
}
